#ifndef MAINFORM_H
#define MAINFORM_H

#include <QWidget>
#include <QUrl>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QFile>
#include <QDir>
#include <QByteArray>
#include <QSettings>
#include <QProcess>
#include <QTextStream>

#include "downloadmanager.h"
#include "paths.h"
#include "checksetup.h"

namespace Ui {class MainForm;}

class MainForm : public QWidget
{
    Q_OBJECT

    public:
        explicit MainForm(QWidget *parent = 0);
        ~MainForm();

        void doWork();

    private slots:
        void on_btn_cancel_clicked();

    private:
        Ui::MainForm *ui;

        bool LoadTes3mp(const QString &path);
        void LoadApi();
        bool LoadPlugin(const QString &path);
        bool LoadContent(const QString &path);
        bool selfUpdate();
        bool processWizard(const QString &path);

        QString m_programver;
        QString m_programcrc;
        QString m_ver;
        QString m_crc;
        QString m_tes3mpver;
        QString m_tes3mpverprefix;

        DownloadManager *manager;

        bool canceled;
};

#endif // MAINFORM_H
