#ifndef PATHS_H
#define PATHS_H

#include <QString>
#include <QDir>
#include <QDesktopServices>

namespace UpdaterPath
{
    extern QString omwpath;
    extern QString apiurl;
    extern QString server;
    extern QString pluginPrefix;
    extern QString freedomlandcfg;
    extern QString openmwcfg;
    extern QString tes3mplink;
    extern QString tes3mparchive;
    extern QString programver;
}

#endif // PATHS_H
