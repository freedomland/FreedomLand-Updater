#include "paths.h"

namespace UpdaterPath
{
#ifdef __linux__
    QString omwpath = QDir::homePath() + "/.config/openmw";
#else
    QString omwpath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/My Games/OpenMW";
#endif

    QString openmwcfg = omwpath + "/openmw.cfg";
    QString freedomlandcfg = omwpath + "/freedomland.cfg";

    QString server = "http://46.29.160.161:25564/tes3mp/";
    QString apiurl = server + "api_v1.0.json";

    QString pluginPrefix = "FreedomLand_";

    QString tes3mplink = "https://github.com/TES3MP/openmw-tes3mp/releases/download/";

    QString programver = "0.1-b4";
}
