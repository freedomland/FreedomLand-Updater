#include "mainform.h"
#include "ui_mainform.h"

#include <QDebug>

MainForm::MainForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);

    manager = new DownloadManager(this);
    connect(manager, &DownloadManager::progress, ui->pb_progress, &QProgressBar::setValue);

    canceled = false;
}

MainForm::~MainForm()
{
    delete manager;
    delete ui;
}

bool MainForm::LoadTes3mp(const QString &path)
{
    ui->l_task->setText("скачиваю клиент...");

    #ifdef __linux__
    QUrl link = UpdaterPath::tes3mplink + "tes3mp-" +\
            m_tes3mpver + "/tes3mp-GNU.Linux-x86_64-release-" +\
            m_tes3mpverprefix + ".tar.gz";

    QFile file(path + "/tes3mp.tar.gz");
    #else
    QUrl link = UpdaterPath::tes3mplink + "tes3mp-" +\
            m_tes3mpver + "/tes3mp.Win64.release." +\
            m_tes3mpverprefix + ".zip";

    QFile file(path + "/tes3mp.zip");
    #endif

    if (!file.exists())
    {
        if (!file.open(QFile::WriteOnly))
        {
            file.close();
            ui->l_task->setText("Не могу записать файл.");
            return false;
        }

        manager->downloadFile(link, &file);

        file.close();
    }

    QProcess *process = new QProcess(this);
    #ifdef __linux__
    process->execute("tar -xf " + file.fileName() + " -C " + path);
    #else
    process->execute(qApp->applicationDirPath() + "/unzip \"" + file.fileName() + "\" -d \"" + path + "/TES3MP\"");
    #endif
    process->waitForFinished();
    delete process;

    return true;
}

void MainForm::LoadApi()
{
    ui->l_task->setText("обрабатываю запрос api...");

    QByteArray body = manager->download(QUrl(UpdaterPath::apiurl));

    QJsonDocument jsonResponse = QJsonDocument::fromJson(body);
    m_ver = jsonResponse.object()["lastPluginVer"].toString();
    m_crc = jsonResponse.object()["pluginCRC"].toString();
    m_tes3mpver = jsonResponse.object()["tes3mpVer"].toString();
    m_tes3mpverprefix = jsonResponse.object()["tes3mpVerPrefix"].toString();
    m_programver = jsonResponse.object()["programVer"].toString();
    m_programcrc = jsonResponse.object()["programCRC"].toString();
}

bool MainForm::LoadPlugin(const QString &path)
{
    ui->l_task->setText("скачиваю плагин...");

    if (m_ver == "")
    {
        ui->l_task->setText("Произошла ошибка сети.");
        return false;
    }

    QUrl link = UpdaterPath::server + UpdaterPath::pluginPrefix + m_ver + ".esp";

    QFile file(path + "/" + UpdaterPath::pluginPrefix + m_ver + ".esp");

    if (file.open(QFile::WriteOnly))
    {
        manager->downloadFile(link, &file);
        file.copy(path + "/" + UpdaterPath::pluginPrefix + m_ver + ".esp",\
                  CheckSetup::getDataFiles() + "/" + UpdaterPath::pluginPrefix + m_ver + ".esp");
        file.remove();
        file.close();
    }
    else
    {
        file.close();
        ui->l_task->setText("Не могу записать файл.");
        return false;
    }

    return true;
}

bool MainForm::LoadContent(const QString &path)
{
    QMessageBox *msg = new QMessageBox(this);

    msg->setText("Установка Morrowind не найдена. "
                "Так как она опирается на заначение в openmw.cfg, то это может быть ложным "
                "срабатыванием, но всё же, если вам необходим Morrowind нажмите 'Ок'. "
                "Если Morrowind у вас уже есть, то нажмите 'Отмена' и обновление продолжится.");

    msg->setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);

    int res = msg->exec();

    if (res == QMessageBox::Ok)
    {
        ui->l_task->setText("cкачиваю контент...");

        #ifdef __linux__
        QFile file(path + "/content.tar.gz");
        #else
        QFile file(path + "/content.zip");
        #endif

        if (!file.exists())
        {
            if (!file.open(QFile::WriteOnly))
            {
                file.close();
                ui->l_task->setText("Не могу записать файл.");
                return false;
            }

            #ifdef __linux__
            manager->downloadFile(QUrl(UpdaterPath::server + "content.tar.gz"), &file);
            #else
            manager->downloadFile(QUrl(UpdaterPath::server + "content.zip"), &file);
            #endif

            file.close();
        }

        QProcess *process = new QProcess(this);

        #ifdef __linux__
        process->execute("tar -xv " + file.fileName() + " -C " + path + "/Morrowind");
        process->waitForFinished();
        #else
        process->execute(qApp->applicationDirPath() + "/unzip \"" + file.fileName() + "\" -d \"" + path + "/Morrowind\"");
        process->waitForFinished();
        #endif

        delete process;

        //QFile content("content.tar");
        //if (content.exists())
        //    content.remove();
    }

    return true;
}

bool MainForm::selfUpdate()
{
    ui->l_task->setText("обновляюсь...");

    if (m_programver == "")
    {
        ui->l_task->setText("Произошла ошибка сети.");
        return false;
    }

    #ifdef __linux__
    QUrl link = UpdaterPath::server + "/updater_linux_x86_64_" + m_programver + ".tar.gz";
    QFile file(qApp->applicationDirPath() + "/" + "updater_linux_x86_64_" + m_programver + ".tar.gz");
    #else
    QUrl link = UpdaterPath::server + "/updater_win64_" + m_programver + ".exe";
    QFile file(qApp->applicationDirPath() + "/" + "updater_win64_" + m_programver + ".exe");
    #endif

    if (file.open(QFile::WriteOnly))
    {
        manager->downloadFile(link, &file);
        file.close();

        #ifdef __linux__
        QProcess *process = new QProcess(this);
        process->execute("tar -xf " + file.fileName());
        process->waitForFinished();
        delete process;
        file.remove();
        #else
        QProcess::startDetached("updater_win64_" + m_programver + ".exe -y");
        #endif
    }
    else
    {
        file.close();
        ui->l_task->setText("Не могу записать файл.");
        return false;
    }

    return true;
}

bool MainForm::processWizard(const QString &path)
{
    if (!CheckSetup::checkOpenmwInstalled())
    {
        QMessageBox *msg = new QMessageBox(this);
        msg->setText("Ошибка! Вы не завершили настройку OpenMW! Дальнейшая установка невозможна. Нажмите 'Ок' чтобы запустить установку ещё раз или 'Отмена' для отмены установки.");
        msg->setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        int result = msg->exec();

        if (result == QMessageBox::Ok)
        {
            QProcess *process = new QProcess(this);
            process->execute(path + "/openmw-wizard");
            process->waitForFinished();
            processWizard(path);
        }
        else
        {
            manager->cancel();
            return false;
        }
    }

    return true;
}

void MainForm::doWork()
{
    QFile uh(qApp->applicationDirPath() + "/updater_win64_" + m_programver + ".exe");
    if (uh.exists())
        uh.remove();

    bool result = true;

    QSettings settings(UpdaterPath::freedomlandcfg, QSettings::IniFormat);
    QString path = settings.value("path").toString();

    LoadApi();

    QString p = path;
    QDir dir(path + "/TES3MP/");

    if (dir.exists())
        p += "/TES3MP";

    if (m_programver == UpdaterPath::programver)
    {
        QFile tes3mpdir(p + "/tes3mp-client-default.cfg");

        if (!tes3mpdir.exists())
            result = LoadTes3mp(path);

        if (!CheckSetup::checkMorrowindInstalled() && result && !canceled)
            result = LoadContent(path);

        if (!CheckSetup::checkOpenmwInstalled() && result && !canceled)
        {
            QProcess *process = new QProcess(this);
            process->execute("\"" + p + "/openmw-wizard\"");
            process->waitForFinished();
            //result = processWizard(p);
        }

        if (result && !canceled)
            CheckSetup::checkOrder(m_ver);

        bool ok = false;
        if (!CheckSetup::checkPluginVersion(m_ver, QString(m_crc).toUInt(&ok, 16)) && result && !canceled)
            result = LoadPlugin(path);

        if (result && !canceled)
        {
            QDir::setCurrent(p);
            QProcess::startDetached("./tes3mp --connect 46.29.160.161:25565");
            this->close();
        }
    }
    else
    {
        result = selfUpdate();

        #ifdef __linux__
        if (result && !canceled)
            QProcess::startDetached(qApp->applicationDirPath() + "/FreedomLand_Updater");
        #endif
    }

    if (!result && canceled)
        QMessageBox::warning(this, "Ошибка!", "Установка была отменена.", QMessageBox::Ok);
}

void MainForm::on_btn_cancel_clicked()
{
    canceled = true;
    manager->cancel();
    ui->pb_progress->setValue(0);
    qApp->quit();
}
