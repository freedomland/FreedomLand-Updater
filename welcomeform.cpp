#include "welcomeform.h"
#include "ui_welcomeform.h"

WelcomeForm::WelcomeForm(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::WelcomeForm)
{
    ui->setupUi(this);

    connect(ui->le_path, &QLineEdit::textChanged, this, &WelcomeForm::enabledButton);
}

WelcomeForm::~WelcomeForm()
{
    delete ui;
}

void WelcomeForm::on_btn_search_clicked()
{
    QString folderName = QFileDialog::getExistingDirectory(this, "Выбирете или создайте новую директорию");
    ui->le_path->setText(folderName);
}

void WelcomeForm::on_btn_cancel_clicked()
{
    this->done(0);
}

void WelcomeForm::enabledButton()
{
    if (ui->le_path->text().isEmpty())
        ui->btn_done->setEnabled(false);
    else
        ui->btn_done->setEnabled(true);
}

void WelcomeForm::on_btn_done_clicked()
{
    QDir path(ui->le_path->text());
    if (!path.exists())
    {
        QMessageBox::warning(this, "Ошибка!",\
                             "Данный путь не существует. Если вы устанавливаете"
                             " в новую директорию, пожайлуста создайте её.", QMessageBox::Ok);

        ui->le_path->focusWidget();

        return;
    }

    QSettings settings(UpdaterPath::freedomlandcfg, QSettings::IniFormat);

    settings.setValue("path", ui->le_path->text());

    this->done(1);
}
