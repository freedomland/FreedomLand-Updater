/********************************************************************************
** Form generated from reading UI file 'welcomeform.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WELCOMEFORM_H
#define UI_WELCOMEFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WelcomeForm
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLineEdit *le_path;
    QPushButton *btn_search;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btn_cancel;
    QPushButton *btn_done;

    void setupUi(QWidget *WelcomeForm)
    {
        if (WelcomeForm->objectName().isEmpty())
            WelcomeForm->setObjectName(QStringLiteral("WelcomeForm"));
        WelcomeForm->resize(511, 241);
        verticalLayout = new QVBoxLayout(WelcomeForm);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(WelcomeForm);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignJustify|Qt::AlignVCenter);
        label->setWordWrap(true);
        label->setMargin(0);
        label->setIndent(-1);

        verticalLayout->addWidget(label);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_2 = new QLabel(WelcomeForm);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        le_path = new QLineEdit(WelcomeForm);
        le_path->setObjectName(QStringLiteral("le_path"));

        horizontalLayout->addWidget(le_path);

        btn_search = new QPushButton(WelcomeForm);
        btn_search->setObjectName(QStringLiteral("btn_search"));

        horizontalLayout->addWidget(btn_search);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        btn_cancel = new QPushButton(WelcomeForm);
        btn_cancel->setObjectName(QStringLiteral("btn_cancel"));

        horizontalLayout_2->addWidget(btn_cancel);

        btn_done = new QPushButton(WelcomeForm);
        btn_done->setObjectName(QStringLiteral("btn_done"));
        btn_done->setEnabled(false);

        horizontalLayout_2->addWidget(btn_done);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(WelcomeForm);

        QMetaObject::connectSlotsByName(WelcomeForm);
    } // setupUi

    void retranslateUi(QWidget *WelcomeForm)
    {
        WelcomeForm->setWindowTitle(QApplication::translate("WelcomeForm", "Freedom Land - \320\277\320\265\321\200\320\262\321\213\320\271 \320\267\320\260\320\277\321\203\321\201\320\272", Q_NULLPTR));
        label->setText(QApplication::translate("WelcomeForm", "<html><head/><body><p>\320\224\320\276\320\261\321\200\320\276 \320\277\320\276\320\266\320\260\320\273\320\276\320\262\320\260\321\202\321\214 \320\262 \320\274\320\260\321\201\321\202\320\265\321\200 \320\276\320\261\320\275\320\276\320\262\320\273\320\265\320\275\320\270\321\217 \320\270 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\272\320\270 \320\272\320\273\320\270\320\275\320\265\321\202\320\260 <span style=\" color:#d19800;\">Freedom Land</span>! \320\237\320\276\321\205\320\276\320\266\320\265 \321\207\321\202\320\276 \320\262\321\213 \320\267\320\260\320\277\321\203\321\201\320\272\320\260\320\265\321\202\320\265 \320\274\320\260\321\201\321\202\320\265\321\200 \320\262 \320\277\320\265\321\200\320\262\321\213\320\265. \320\225\321\201\320\273\320\270 \321\203 \320\262\320\260\321\201 \321\203\320\266\320\265 \320\265\321\201\321\202\321\214 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\273\320\265\320\275\320\275\321\213\320\271 \320\272\320\273\320\270\320\265\320\275"
                        "\321\202 tes3mp, \321\202\320\276, \320\277\320\276\320\266\320\260\320\271\320\273\321\203\321\201\321\202\320\260 \321\203\320\272\320\260\320\266\320\270\321\202\320\265 \320\277\321\203\321\202\321\214 \320\264\320\276 \320\275\320\265\320\263\320\276. \320\225\321\201\320\273\320\270 \320\266\320\265 \320\275\320\265\321\202, \321\202\320\276 \320\274\320\260\321\201\321\202\320\265\321\200 \321\201\320\264\320\265\320\273\320\260\320\265\321\202 \320\262\321\201\321\221 \321\201\320\260\320\274, \320\262\320\260\320\274 \320\275\320\265\320\276\320\261\321\205\320\276\320\264\320\270\320\274\320\276 \321\202\320\276\320\273\321\214\320\272\320\276 \321\203\320\272\320\260\320\267\320\260\321\202\321\214 \320\266\320\265\320\273\320\260\320\265\320\274\321\203\321\216 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\321\216 \320\264\320\273\321\217 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\272\320\270.</p><p>\320\222 \320\264\320\260\320\273\321\214\320\275\320\265"
                        "\320\271\321\210\320\265\320\274 \321\215\321\202\320\276\321\202 \320\264\320\270\320\260\320\273\320\276\320\263 \320\275\320\265 \320\277\320\276\321\217\320\262\320\270\321\202\321\201\321\217, \320\275\320\276 \320\262\320\260\320\274 \320\275\320\265\320\276\321\205\320\276\320\264\320\270\320\274\320\276 \320\267\320\260\320\277\321\203\321\201\320\272\320\260\321\202\321\214 \321\215\321\202\320\276\321\202 \320\270\321\201\320\277\320\276\320\273\320\275\321\217\320\271\320\274\321\213\320\271 \321\204\320\260\320\271\320\273 \321\207\321\202\320\276\320\261\321\213 \321\201\320\262\320\276\320\265\320\262\321\200\320\265\320\274\320\265\320\275\320\275\320\276 \320\277\320\276\320\273\321\203\321\207\320\260\321\202\321\214 \320\276\320\261\320\275\320\276\320\262\320\273\320\265\320\275\320\270\321\217.</p></body></html>", Q_NULLPTR));
        label_2->setText(QApplication::translate("WelcomeForm", "\320\237\321\203\321\201\321\202\321\214 \320\272 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\272\320\265 tes3mp: ", Q_NULLPTR));
        btn_search->setText(QApplication::translate("WelcomeForm", "\320\236\320\261\320\267\320\276\321\200", Q_NULLPTR));
        btn_cancel->setText(QApplication::translate("WelcomeForm", "\320\236\321\202\320\274\320\265\320\275\320\260", Q_NULLPTR));
        btn_done->setText(QApplication::translate("WelcomeForm", "\320\223\320\276\321\202\320\276\320\262\320\276", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WelcomeForm: public Ui_WelcomeForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WELCOMEFORM_H
