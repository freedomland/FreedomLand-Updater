#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QByteArray>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QMessageBox>
#include <QFile>

class DownloadManager : public QObject
{
    Q_OBJECT

    public:
        DownloadManager(QObject *parent = 0);
        virtual ~DownloadManager();

        QByteArray download(QUrl url);
        void downloadFile(QUrl url, QFile *file);

    signals:
        void progress(int p);

    public slots:
        void cancel();

    private:
        QNetworkAccessManager *manager;
        QNetworkReply *reply;

        int m_progress;
};

#endif // DOWNLOADMANAGER_H
