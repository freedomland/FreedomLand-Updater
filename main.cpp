/*
    Freedomland Updater - update or install Freedom Land client setup
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QSettings>
#include <QFile>

#include "welcomeform.h"
#include "mainform.h"
#include "paths.h"
#include "checksetup.h"

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    #ifdef _WIN32
    if (CheckSetup::checkProgram())
        return 0;
    #endif

    int result = 1;

    QFile file(UpdaterPath::freedomlandcfg);
    WelcomeForm welcome;
    MainForm form;

    if (!file.exists())
    {
        result = welcome.exec();
    }
    else
    {
        QSettings settings(UpdaterPath::freedomlandcfg, QSettings::IniFormat);
        QString path = settings.value("path").toString();

        QFile t3path(path);

        if (path.isEmpty() || !t3path.exists())
            result = welcome.exec();
    }

    if (result == 1)
    {
        form.show();
        form.doWork();
    }

    //return app.exec();
    return 0;
}
