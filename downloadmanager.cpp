#include "downloadmanager.h"

DownloadManager::DownloadManager(QObject *parent) : QObject(parent){}

DownloadManager::~DownloadManager()
{
    //delete reply;
    //delete manager;
}

QByteArray DownloadManager::download(QUrl url)
{
    manager = new QNetworkAccessManager(this);
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    reply = manager->get(request);

    connect(reply, &QNetworkReply::downloadProgress, [&](qint64 bytesReceived, qint64 bytesTotal) {
        emit progress(bytesReceived * 100 / bytesTotal);
    });

    QEventLoop event;
    connect(reply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    if (reply->error() != QNetworkReply::NoError)
    {
        QMessageBox::warning(0, "Ошибка!", reply->errorString(), QMessageBox::Ok);
        return "";
    }

    return reply->readAll();
}

void DownloadManager::downloadFile(QUrl url, QFile *file)
{
    manager = new QNetworkAccessManager(this);
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    reply = manager->get(request);

    connect(reply, &QNetworkReply::downloadProgress, [&](qint64 bytesReceived, qint64 bytesTotal) {
        QByteArray b = reply->readAll();
        QDataStream out(file);
        out.writeRawData(b, b.size());
        emit progress(bytesReceived * 100 / bytesTotal);
    });

    QEventLoop event;
    connect(reply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    if (reply->error() != QNetworkReply::NoError)
    {
        QMessageBox::warning(0, "Ошибка!", reply->errorString(), QMessageBox::Ok);
        return;
    }
}

void DownloadManager::cancel()
{
    reply->abort();
}
