#ifndef CHECKSETUP_H
#define CHECKSETUP_H

#include <iostream>
#include <vector>
#include <fstream>

#include <QSettings>
#include <QFile>
#include <QDir>
#include <QString>
#include <QVector>
#include <QTextStream>
#include <QProcess>
#include <QDate>
#include <QMessageBox>
#include <QApplication>

#include "ftlip/ftlip.h"
#include "crc/crc.h"

#include "paths.h"

class CheckSetup
{
    public:
        static bool checkPluginVersion(QString curVersion, unsigned int crc32);
        static bool checkOpenmwInstalled();
        static bool checkMorrowindInstalled();
        static void checkOrder(const QString &curPlugVer);
        static QString getDataFiles();
        static bool checkProgram();

    private:
        CheckSetup();

        static void removeContent(const QString &curPlugVer, bool makecopy);
};

#endif // CHECKSETUP_H
