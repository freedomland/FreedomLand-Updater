#ifndef WELCOMEFORM_H
#define WELCOMEFORM_H

#include <QDialog>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QSettings>

#include "paths.h"

namespace Ui {class WelcomeForm;}

class WelcomeForm : public QDialog
{
    Q_OBJECT

    public:
        explicit WelcomeForm(QDialog *parent = 0);
        ~WelcomeForm();

    private slots:
        void on_btn_search_clicked();
        void on_btn_cancel_clicked();
        void on_btn_done_clicked();

private:
        Ui::WelcomeForm *ui;

        void enabledButton();
};

#endif // WELCOMEFORM_H
