#include "checksetup.h"

#include <QDebug>

bool CheckSetup::checkProgram()
{
    QVector<QString> cl = {"libeay32.dll", "libssl32.dll", "ssleay32.dll", "msvcr120.dll", "unzip.exe", "unzip32.dll"};

    for (auto item : cl)
    {
        QFile file(QApplication::applicationDirPath() + "/" + item);

        if (!file.exists())
        {
            QMessageBox::warning(0, "Ошибка!", "Файл " + item + " отсутсвует в установке! Пожайлуста скачайте программу заново.", QMessageBox::Ok);
            return true;
        }
    }

    return false;
}

bool CheckSetup::checkMorrowindInstalled()
{
    if (!checkOpenmwInstalled())
        return false;

    if (!getDataFiles().isEmpty())
        return true;

    return false;
}

QString CheckSetup::getDataFiles()
{
    QString found;

    QString cfg(UpdaterPath::openmwcfg);

    QFile file;

    ftlip ini(cfg.toStdString());
    std::vector<std::string> data = ini.getMult("data");

    for (auto item : data)
    {
        QString tmp = QString::fromStdString(item).remove("\"");

        file.setFileName(tmp + "/Morrowind.esm");
        if (file.exists())
        {
            found = tmp;
            break;
        }
    }

    return found;
}

bool CheckSetup::checkOpenmwInstalled()
{
    QFile cfg(UpdaterPath::openmwcfg);
    return cfg.exists();
}

bool CheckSetup::checkPluginVersion(QString curVersion, unsigned int crc32)
{
    QString pluginPath = getDataFiles();
    QFile file;

    file.setFileName(pluginPath + "/" + UpdaterPath::pluginPrefix + curVersion + ".esp");

    if (file.exists())
    {
        std::string str = pluginPath.toStdString() +\
                "/" + UpdaterPath::pluginPrefix.toStdString() +\
                curVersion.toStdString() + ".esp";

        std::ifstream files(str, std::ios::binary);

        if (crc::crc32file(files) != crc32)
            return false;

        return true;
    }

    return false;
}

void CheckSetup::checkOrder(const QString &curPlugVer)
{
    /*QFile file(UpdaterPath::openmwcfg);
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream in(&file);
        qDebug() << in.readAll();
    }*/

    ftlip ini(UpdaterPath::openmwcfg.toStdString());

    std::vector<std::string> contents = ini.getMult("content");

    if ((contents.at(0) != "Morrowind.esm" \
            && contents.at(1) != "Tribunal.esm" \
            && contents.at(2) != "Bloodmoon.esm") || contents.size() > 4)
    {
        removeContent(curPlugVer, true);
        return;
    }

    if (contents.size() > 3)
    {
        QString tmp = QString::fromStdString(contents.at(3));

        if (!tmp.contains("FreedomLand_"))
        {
            removeContent(curPlugVer, true);
            return;
        }
        else
        {
            if (tmp != "FreedomLand_" + curPlugVer + ".esp")
            {
                removeContent(curPlugVer, false);
                return;
            }

            //qDebug() << "Everything seams in order. Skip.";
            return;
        }
    }
    else
    {
        QFile file(UpdaterPath::openmwcfg);
        if (file.open(QIODevice::ReadWrite | QIODevice::Append))
        {
            QTextStream stream(&file);
            stream << "\ncontent=FreedomLand_" + curPlugVer + ".esp" << endl;
        }
        file.close();
    }
}

void CheckSetup::removeContent(const QString &curPlugVer, bool makecopy)
{
    QFile file(UpdaterPath::openmwcfg);

    if (makecopy)
    {
        file.copy(UpdaterPath::openmwcfg + ".bak-" + QDate::currentDate().toString());

        /*QSettings settings(UpdaterPath::omwpath + "/launcher.cfg", QSettings::IniFormat);
        settings.beginGroup("Profiles");
        settings.value("currentprofile", "openmw-" + QDate::currentDate().toString());
        settings.endGroup();*/
    }

    QString tmp = "";
    if (!file.open(QIODevice::ReadOnly| QIODevice::Text))
        return;

    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString line = in.readLine();

        if (line.contains("content="))
            continue;

        tmp += line + "\n";
    }

    file.remove();

    if (!file.open(QIODevice::WriteOnly| QIODevice::Text))
        return;

    in << tmp + "\ncontent=Morrowind.esm\ncontent=Tribunal.esm\ncontent=Bloodmoon.esm\ncontent=FreedomLand_" +\
                                                                                        curPlugVer + ".esp";
    file.close();
}
