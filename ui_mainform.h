/********************************************************************************
** Form generated from reading UI file 'mainform.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINFORM_H
#define UI_MAINFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainForm
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *l_task;
    QSpacerItem *horizontalSpacer;
    QProgressBar *pb_progress;
    QPushButton *btn_cancel;

    void setupUi(QWidget *MainForm)
    {
        if (MainForm->objectName().isEmpty())
            MainForm->setObjectName(QStringLiteral("MainForm"));
        MainForm->resize(400, 173);
        verticalLayout = new QVBoxLayout(MainForm);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_2 = new QLabel(MainForm);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setWordWrap(true);

        verticalLayout->addWidget(label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(MainForm);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout->addWidget(label);

        l_task = new QLabel(MainForm);
        l_task->setObjectName(QStringLiteral("l_task"));

        horizontalLayout->addWidget(l_task);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        pb_progress = new QProgressBar(MainForm);
        pb_progress->setObjectName(QStringLiteral("pb_progress"));
        pb_progress->setValue(0);

        verticalLayout->addWidget(pb_progress);

        btn_cancel = new QPushButton(MainForm);
        btn_cancel->setObjectName(QStringLiteral("btn_cancel"));

        verticalLayout->addWidget(btn_cancel);


        retranslateUi(MainForm);

        QMetaObject::connectSlotsByName(MainForm);
    } // setupUi

    void retranslateUi(QWidget *MainForm)
    {
        MainForm->setWindowTitle(QApplication::translate("MainForm", "Freedom Land - \320\276\320\261\320\275\320\276\320\262\320\273\320\265\320\275\320\270\320\265", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainForm", "<html><head/><body><p>\320\255\321\202\320\276 \320\276\320\272\320\275\320\276 \320\267\320\260\320\272\321\200\320\276\320\265\320\272\321\202\321\201\321\217 \320\260\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\270 \320\277\320\276\321\201\320\273\320\265 \320\262\321\213\320\277\320\276\320\273\320\275\320\265\320\275\320\270\321\217 \320\262\321\201\320\265\321\205 \320\267\320\264\320\260\321\207. \320\234\320\276\320\266\320\265\321\202 \320\272\320\260\320\267\320\260\321\202\321\214\321\201\321\217 \321\207\321\202\320\276 \320\276\320\275\320\276 &quot;\320\267\320\260\320\262\320\270\321\201\320\273\320\276&quot;, \320\275\320\276 \320\275\320\260 \321\201\320\260\320\274\320\276\320\274 \320\264\320\265\320\273\320\265 \321\215\321\202\320\276 \320\275\320\265 \321\202\320\260\320\272. \320\221\321\203\320\264\321\202\320\265 \321\202\320\265\321\200\320\277\320\265\320\273\320\270\320\262\320\265\320\265 :)</p></body></html>", Q_NULLPTR));
        label->setText(QApplication::translate("MainForm", "\320\242\320\272\321\203\321\211\320\260\321\217 \320\267\320\260\320\264\320\260\321\207\320\260:", Q_NULLPTR));
        l_task->setText(QString());
        btn_cancel->setText(QApplication::translate("MainForm", "\320\236\321\202\320\274\320\265\320\275\320\260", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainForm: public Ui_MainForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINFORM_H
